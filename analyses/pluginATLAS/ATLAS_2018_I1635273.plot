# BEGIN PLOT /ATLAS_2018_I1635273/d..
LogY=1
LegendAlign=r
XTwosidedTicks=1
YTwosidedTicks=1
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d..-x01-y03
LogY=0
LegendAlign=l
LegendXPos=0.05
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d0[1,3]-.*
XLabel=$N_\text{jets}$
XMinorTickMarks=0
XCustomMajorTicks=0  $\geq0$  1  $\geq1$  2  $\geq2$  3  $\geq3$  4  $\geq4$  5  $\geq5$  6  $\geq6$  7  $\geq7$  
YLabel=$\sigma(W + N_\text{jets})$ [pb]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d01-x01-y01
Title=$W^\pm + \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d03-x01-y01
Title=$W^+ + \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d03-x01-y02
Title=$W^- + \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d03-x01-y03
YLabel=$\sigma(W^+ + N_\text{jets}) / \sigma(W^- + N_\text{jets})$
Title=$W^+ + \text{jets} / W^- + \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d0[6,8]-.*
XLabel=$H_\text{T}$ [GeV]
YLabel=$\text{d}\sigma / \text{d} H_\text{T})$ [fb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d06-x01-y01
Title=$W^\pm + \geq 0 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d08-x01-y01
Title=$W^+ + \geq 0 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d08-x01-y02
Title=$W^- + \geq 0 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d08-x01-y03
YLabel=$\text{d}\sigma^{W^+} / \text{d} H_\text{T} / \text{d}\sigma^{W^-} / \text{d} H_\text{T}$ 
Title=$W^+ + \geq 0 \text{jets} / W^- + \geq 0 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d1[1,3]-.*
XLabel=$W$ boson $p_\text{T}$ [GeV]
YLabel=$\text{d}\sigma / \text{d} p^W_\text{T})$ [fb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d11-x01-y01
Title=$W^\pm + \geq 1 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d13-x01-y01
Title=$W^+ + \geq 1 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d13-x01-y02
Title=$W^- + \geq 1 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d13-x01-y03
YLabel=$\text{d}\sigma^{W^+} / \text{d} p^W_\text{T} / \text{d}\sigma^{W^-} / \text{d} p^W_\text{T}$ 
Title=$W^+ + \geq 1 \text{jets} / W^- + \geq 1 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d1[6,8]-.*
XLabel=Leading jet $p_\text{T}$ [GeV]
YLabel=$\text{d}\sigma / \text{d} p^\text{jet1}_\text{T})$ [fb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d16-x01-y01
Title=$W^\pm + \geq 1 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d18-x01-y01
Title=$W^+ + \geq 1 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d18-x01-y02
Title=$W^- + \geq 1 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d18-x01-y03
YLabel=$\text{d}\sigma^{W^+} / \text{d} p^\text{jet1}_\text{T} / \text{d}\sigma^{W^-} / \text{d} p^\text{jet1}_\text{T}$ 
Title=$W^+ + \geq 1 \text{jets} / W^- + \geq 1 \text{jets}$ (electron channel data)
# END PLOT


# BEGIN PLOT /ATLAS_2018_I1635273/d2[1,3]-.*
XLabel=Leading jet $|y|$
YLabel=$\text{d}\sigma / \text{d} |y^\text{jet1}|$ [fb]
LegendAlign=l
LegendXPos=0.05
LegendYPos=0.35
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d21-x01-y01
Title=$W^\pm + \geq 1 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d23-x01-y01
Title=$W^+ + \geq 1 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d23-x01-y02
Title=$W^- + \geq 1 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d23-x01-y03
YLabel=$\text{d}\sigma^{W^+} / \text{d} |y^\text{jet1}| / \text{d}\sigma^{W^-} / \text{d} |y^\text{jet1}|$ 
Title=$W^+ + \geq 1 \text{jets} / W^- + \geq 1 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d26-.*
XLabel=Second leading jet $p_\text{T}$ [GeV]
YLabel=$\text{d}\sigma / \text{d} p^\text{jet2}_\text{T})$ [fb/GeV]
Title=$W^\pm + \geq 2 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d28-.*
XLabel=Second leading jet $|y|$
YLabel=$\text{d}\sigma / \text{d} |y^\text{jet2}|$ [fb]
Title=$W^\pm + \geq 2 \text{jets}$ (electron channel data)
LegendAlign=l
LegendXPos=0.05
LegendYPos=0.35
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d30-.*
XLabel=$\Delta R_\text{jet1,jet2}$
YLabel=$\text{d}\sigma / \text{d} \Delta R_\text{jet1,jet2})$ [fb]
Title=$W^\pm + \geq 2 \text{jets}$ (electron channel data)
LegendAlign=l
LegendXPos=0.05
LegendYPos=0.35
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d32-.*
XLabel=$m_\text{jet1,jet2}$ [GeV]
YLabel=$\text{d}\sigma / \text{d} m_\text{jet1,jet2})$ [fb/GeV]
Title=$W^\pm + \geq 2 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d34-.*
XLabel=$N_\text{jets}$
XMinorTickMarks=0
YLabel=$\sigma(W + N_\text{jets})$ [pb]
Title=$W^\pm + \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d3[6,8]-.*
XLabel=$H_\text{T}$ [GeV]
YLabel=$\text{d}\sigma / \text{d} H_\text{T})$ [fb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d36-x01-y01
Title=$W^\pm + \geq 2 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d38-x01-y01
Title=$W^+ + \geq 2 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d38-x01-y02
Title=$W^- + \geq 2 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d38-x01-y03
Title=$W^+ + \geq 2 \text{jets} / W^- + \geq 2 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d4[1,3]-.*
XLabel=$W$ boson $p_\text{T}$ [GeV]
YLabel=$\text{d}\sigma / \text{d} p^W_\text{T})$ [fb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d41-x01-y01
Title=$W^\pm + \geq 2 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d43-x01-y01
Title=$W^+ + \geq 2 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d43-x01-y02
Title=$W^- + \geq 2 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d43-x01-y03
Title=$W^+ + \geq 2 \text{jets} / W^- + \geq 2 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d4[6,8]-.*
XLabel=Leading jet $p_\text{T}$ [GeV]
YLabel=$\text{d}\sigma / \text{d} p^\text{jet1}_\text{T})$ [fb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d46-x01-y01
Title=$W^\pm + \geq 2 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d48-x01-y01
Title=$W^+ + \geq 2 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d48-x01-y02
Title=$W^- + \geq 2 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d48-x01-y03
Title=$W^+ + \geq 2 \text{jets} / W^- + \geq 2 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d5[1,3,6,8]-x..-y..
LogY=0
XLabel=Dressed lepton $|\eta|$
YLabel=$\text{d}\sigma / \text{d} |\eta^\ell|$ [fb]
LegendAlign=l
LegendXPos=0.05
LegendYPos=0.35
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d51-x01-y01
Title=$W^\pm + \geq 0 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d5[3,8]-x01-y03
YLabel=$\text{d}\sigma^{W^+} / \text{d} |\eta^\ell| / \text{d}\sigma^{W^-} / \text{d} |\eta^\ell|$
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d53-x01-y01
Title=$W^+ + \geq 0 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d53-x01-y02
Title=$W^- + \geq 0 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d53-x01-y03
Title=$W^+ + \geq 0 \text{jets} / W^- + \geq 0 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d56-x01-y01
Title=$W^\pm + \geq 1 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d58-x01-y01
Title=$W^+ + \geq 1 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d58-x01-y02
Title=$W^- + \geq 1 \text{jets}$ (electron channel data)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1635273/d58-x01-y03
Title=$W^+ + \geq 1 \text{jets} / W^- + \geq 1 \text{jets}$ (electron channel data)
# END PLOT

